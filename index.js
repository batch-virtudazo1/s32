let http = require("http");

// / Mock collection of courses:
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}


];

http.createServer(function(request,response){
	console.log(request.url);
	console.log(request.method);
/*
	Http request are differentiate not only via endpoints but also with their methods.

	Http Methods simply tells the server what action it must take or what kind of response
	is needed for the request.

	With http mehthods we can actually create routes with the same endpoint but with different methods.

	Browser client is only limited by GET method only.
*/
	
	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. GET Method request.");

	} else if (request.url === "/" && request.method === "POST"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. POST Method request.");

	} else if (request.url === "/" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. PUT Method request.");

	}else if (request.url === "/" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. DELETE Method request.");

	} else if (request.url === "/courses" && request.method === "GET"){
		/*
			When sending a JSON format data as response, change the Content-Type of the
			response to application/json
		*/
		response.writeHead(200,{'Content-Type':'application/json'});
		response.end(JSON.stringify(courses));

	} else if (request.url === "/courses" && request.method === "POST"){
		/*
			We have to receive an input from the client.
			To be able to receive a request body from the request, we have to
		*/
		let requestBody = "";

		// 1st step in receiving data from the request in NodeJS is called data step.
		
		// Data step will read the incoming stream of data from the client and process
		// it so we can save it in the requestBody variable.


		request.on('data',function(data) {
			// console.log(data);
			// the data stream is stored in the variable as string.
			requestBody += data;

		})

		// end step. this will run once or after the request data has been
		// completely sent from the client.
		request.on('end', function(){
		console.log(requestBody);
		
		// update requestBody with a parsed version of the received JSON
		// format data/

		requestBody = JSON.parse(requestBody);
		// add the requestBody in the array.
		courses.push(requestBody);
		// check the courses array if we were able to add our requestBody

		response.writeHead(200,{'Content-Type':'application/json'});
		response.end(JSON.stringify(courses));
		 console.log(courses);
		// console.log(requestBody);

		})

	}

}).listen(4000);
console.log("Server is running on LocalHost: 4000!");